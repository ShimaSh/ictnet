import numpy as np
import cv2
from osgeo import gdal
from osgeo import ogr
import os
import subprocess
import sys
import json
import random
import csv
from config import cfg

shape_file_type = cfg.output_shape_type
gdal_poly_path = cfg.gdal_polygonize_path

def raster_to_shape(in_image, in_path, out_path):    
    in_file = in_path + in_image
    if shape_file_type == 'ESRI Shapefile':
        out_file = out_path + in_image.replace('.tif', '')
        command = ["python", gdal_poly_path + "gdal_polygonize.py", "-8", in_file, "-b", "1", "-f", "ESRI Shapefile", out_file, in_image.replace('.tif', ''), "DN"]
    elif shape_file_type == 'GeoJSON':
        out_file = out_path + in_image.replace('.tif', '.geojson')
        command = ["python", gdal_poly_path + "gdal_polygonize.py", "-8", in_file, "-b", "1", "-f", "GeoJSON", out_file, "None", "DN"]
    # print(command)
    subprocess.call(command)

def convert_raster_to_geoTiff(in_image, in_img_path, in_raster_path, out_path):
    try:
        fileformat = "GTiff"
        dataset = gdal.Open(in_img_path + in_image, gdal.GA_ReadOnly)
        driver = gdal.GetDriverByName(fileformat)
        dst_ds = driver.Create(out_path + in_image, xsize=dataset.RasterXSize, ysize=dataset.RasterYSize, bands=1, eType=gdal.GDT_Byte)
        dst_ds.SetGeoTransform(dataset.GetGeoTransform())
        dst_ds.SetProjection(dataset.GetProjection())        
        img = cv2.imread(in_raster_path + in_image, 0)
        dst_ds.GetRasterBand(1).WriteArray(img)
    except Exception as e:
        print("Error: " + str(e))

def raster_to_shape_dir(in_path, out_path):
    for fi in [x for x in os.listdir(in_path) if x.endswith('.tif')]:
        raster_to_shape(fi, in_path, out_path)

def convert_raster_to_geoTiff_dir(in_img_path, in_raster_path, out_path):
    for fi in [x for x in os.listdir(in_raster_path) if x.endswith('.tif')]:
        convert_raster_to_geoTiff(fi, in_img_path, in_raster_path, out_path)

## Stand alone test code
# in_img_path = '' #path to the source RGB image
# in_raster_path = '' #path to the source Binary image
# out_path = '' #path to output the Geo-referenced image
# convert_raster_to_geoTiff_dir(in_img_path, in_raster_path, out_path)

# in_path = '' #path to the Geo-referenced image
# out_path = '' #path to output the shape files
# raster_to_shape_dir(in_path, out_path)
