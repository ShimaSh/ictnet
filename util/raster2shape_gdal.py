import numpy as np
import cv2
from osgeo import gdal
from osgeo import ogr
import os
import subprocess
import sys
import json
import random
import csv

out_type = "ESRI Shapefile" # output type options ["GeoJSON","ESRI Shapefile"]
gdal_poly_path = "C:/Program Files/QGIS 3.4/bin/"

def raster_to_geojson(in_path, out_path):
    for fi in [x for x in os.listdir(in_path) if x.endswith('.tif')]:
        in_file = in_path + fi
        if out_type == 'ESRI Shapefile':
            out_file = out_path + fi.replace('.tif', '')
            command = ["python", gdal_poly_path + "gdal_polygonize.py", "-8", in_file, "-b", "1", "-f", "ESRI Shapefile", out_file, fi.replace('.tif', ''), "DN"]
        elif out_type == 'GeoJSON':
            out_file = out_path + fi.replace('.tif', '.geojson')
            command = ["python", gdal_poly_path + "gdal_polygonize.py", "-8", in_file, "-b", "1", "-f", "GeoJSON", out_file, "None", "DN"]
        print(command)
        subprocess.call(command)
    print("Done!")

def convert_raster_to_geoTiff(in_img_path, in_raster_path, out_path):
    for fi in [x for x in os.listdir(in_raster_path) if x.endswith('.tif')]:
        dataset = gdal.Open(in_img_path + fi.replace('pred','img'), gdal.GA_ReadOnly)
        # print("Driver: {}/{}".format(dataset.GetDriver().ShortName, dataset.GetDriver().LongName))
        # print("Size is {} x {} x {}".format(dataset.RasterXSize, dataset.RasterYSize, dataset.RasterCount))
        print("Projection is {}".format(dataset.GetProjection()))
        geotransform = dataset.GetGeoTransform()
        if geotransform:
            print("Origin = ({}, {})".format(geotransform[0], geotransform[3]))
            print("Pixel Size = ({}, {})".format(geotransform[1], geotransform[5]))
        
        fileformat = "GTiff"
        driver = gdal.GetDriverByName(fileformat)
        dst_ds = driver.Create(out_path + fi, xsize=dataset.RasterXSize, ysize=dataset.RasterYSize, bands=1, eType=gdal.GDT_Byte)
        dst_ds.SetGeoTransform(geotransform)
        dst_ds.SetProjection(dataset.GetProjection())
        
        img = cv2.imread(in_raster_path + fi, 0)
        dst_ds.GetRasterBand(1).WriteArray(img)

in_img_path = '' #path to the source RGB image
in_raster_path = '' #path to the source Binary image
out_path = '' #path to output the Geo-referenced image
convert_raster_to_geoTiff(in_img_path, in_raster_path, out_path)

in_path = '' #path to the Geo-referenced image
out_path = '' #path to output the shape files
raster_to_geojson(in_path, out_path)
